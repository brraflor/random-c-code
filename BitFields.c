//
//  BitFields.c
//  
//
//  Created by Brian Flores on 12/23/14.
//
//

#include <stdio.h>
#include <string.h>

struct
{
    unsigned int widthValidated;
    unsigned int heightValidated;
} stat1;

struct
{
    unsigned int widthValidated : 1;
    unsigned int heightValidated : 2;
}stat2;

    int main()
{
    printf( "Memory size occupied by stat1 : %d\n", sizeof(stat1));
    printf( "Memory size occupied by stat2 : %d\n", sizeof(stat2));
           
           return 0;
}
