//
//  Pointer_to_Struct.c
//  
//
//  Created by Brian Flores on 12/23/14.
//
//

#include <stdio.h>
#include <string.h>

struct Books
{
    char  title[50];
    char  author[50];
    char  subject[100];
    int   book_id;
};
    /* function declaration */

void printBook(struct Books *books);

    int main()
{
    struct Books Book1;
    struct Books Book2;
    struct Books Book3;
    
    /* book 1 specifications */
    strcpy (Book1.title, "C Programing");
    strcpy (Book1.author, "Brian Flores");
    strcpy (Book1.subject,"C Programing Tutorial");
    Book1.book_id = 6495407;
    
    /* book 2 specification */
    strcpy( Book2.title, "Telecom Billing");
    strcpy( Book2.author, "Zara Ali");
    strcpy( Book2.subject, "Telecom Billing Tutorial");
    Book2.book_id = 6495700;
    
    /* book 3 specifications */
    strcpy( Book3.title, "XYZ");
    strcpy( Book3.author, "Undeclared");
    strcpy( Book3.subject, "Tutorial");
    Book3.book_id = 6494860;

    
    
    /* print Book1 info by passing address of Book1 */
    printBook( &Book1 );
    
    /* print Book2 info by passing address of Book2 */
    printBook( &Book2 );
    
    /* print Book3 info by passing address of Book3	 */
    printBook( &Book3 );
    
    return 0;

}
void printBook( struct Books *book )
{
    printf( "Book title : %s\n", book->title);
    printf( "Book author : %s\n", book->author);
    printf( "Book subject : %s\n", book->subject);
    printf( "Book book_id : %d\n", book->book_id);
}