#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef unsigned char uchar_t;
typedef struct foo{
    int bar;
    char *baz;
    char quux;
} foo;

void printarray (char *label, foo *array, size_t nelem) {
    for (size_t itor = 0; itor < nelem; ++itor) {
        printf ("%s[%2ld] = {%d, \"%s\", \'%c\'}\n", label, itor,
                array[itor].bar, array[itor].baz, array[itor].quux);
    }
}

int foocmp (const void *this, const void *that) {
    foo *thisfoo = (struct foo *) this;
    foo *thatfoo = (struct foo *) that;
    int diff = (thisfoo->bar > thatfoo->bar)
    - (thisfoo->bar < thatfoo->bar);
    if (diff != 0) return diff;
    diff = strcmp (thisfoo->baz, thatfoo->baz);
    if (diff != 0) return diff;
    return (uchar_t) thisfoo->quux - (uchar_t) thatfoo->quux;
}

int main (int argc, char **argv) {
    printf ("Running %s with %c args\n", *argv, argc);
    
    foo array[] = {
        {23, "baz", 'a'},
        {69, "baz", 'a'},
        {23, "foo", 'p'},
        {23, "baz", 'p'},
        {23, "bar", 'p'},
        {66, "bar", 'x'},
        {66, "baz", 'x'},
        {69, "baz", 'x'},
        {34, "foo", 'x'},
        {66, "bar", 'z'},
        {34, "foo", 'z'},
    };
    const size_t nelem = sizeof array / sizeof *array;
    
    printarray ("Unsorted", array, nelem);
    qsort (array, nelem, sizeof *array, foocmp);
    printarray ("Sorted", array, nelem);
    
    return EXIT_SUCCESS;
}