//
//  MyLink.c
//  
//
//  Created by Brian Flores on 12/27/14.
//
//

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int info;
    struct node* link;
};

int main()
{
    struct node *start, *list;
    int k;
    
    start = (struct node *)malloc(sizeof( struct node));
    list=start;
    
    start->link=NULL;
    
    for (k=0; k<100; k++)
    {
        list->info=k;
        list->link= (struct node*)malloc(sizeof(struct node));
        list = list->link;
    }
    
    list->link= NULL;
    while (start != NULL)
    {
        printf("%d\n", start->info);
        start = start->link;
    }
}


