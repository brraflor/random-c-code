// $Id: catbyline.c,v 1.5 2014-01-28 17:59:24-08 - - $

//
// NAME Brraflor
//    catbyline - example of simple cat to display files
//
// SYNOPSIS
//    catbyline [filename...]
//
// DESCRIPTION
//    Uses fgets to read lines from files and prints to stdout.
//    If no filenames are specified, reads stdin.  Does not consider
//    whether input buffer is too short.
//


#define _GNU_SOURCE
#include <errno.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>
#include <unistd.h>


char *program_name = NULL;
char *keyword = NULL;
int exit_status = EXIT_SUCCESS;
#define STDIN_NAME "-"

struct options
{
    bool ignore_case;
    bool filenames_only;
    bool number_lines;
};
struct options opts;
//Checks for user option and turns boolean accordingly.
void scanOpts (int argc, char **argv, struct  options *opts)
{
    opts->ignore_case = false;
    opts->filenames_only = false;
    opts->number_lines = false;
    opterr = false;
    for (;;) {
        int opt = getopt(argc, argv, "iln");
        if (opt == EOF) break;
        switch (opt) {
            case 'i':
                opts->ignore_case = true;
                break;
            case 'l':
                opts->filenames_only = true;
                break;
            case 'n':
                opts->number_lines = true;
                break;
            default:
                exit_status = EXIT_FAILURE;
                fflush(NULL);
                fprintf(stderr, "%s: -%c: invalid option\n",
                        program_name, optopt);
                break;
        }
    }
}


void catbyline (FILE *input, struct options *opts,
                char *filename, char **argv, int argi)
{
    char buffer[1024];
    int linenr = 0;
    
    for (;;)
    {
        ++linenr;
        char *buf = fgets (buffer, sizeof buffer, input);
        if (buf == NULL) break;
        if (keyword == NULL) break;
        char *ptr;
        if (opts->ignore_case == false)
        {
            ptr = strstr(buffer, keyword);
        }else if (opts->ignore_case == true)
        {
            ptr = strcasestr(buffer, keyword);
        }
        
        if (opts->filenames_only == false)
        {
            if (ptr != NULL)
            {
                
                if (argv[optind+2] != NULL) printf("%s:", argv[argi]);
                
                if (opts->number_lines) printf("%d:", linenr);
                fputs(buffer, stdout);
            }
        }else
        {
            printf("%s\n", filename);
            return;
        }
    }
}


int main (int argc, char **argv)
{   //If no input then print error
    if (argc == 1)
    {
     printf("Usage: cmatch [-iln] string [filename...]\n");
    }
    //Store the program name
    //program_name = basename(argv[0]);
    // activate scanOpts
    scanOpts(argc, argv, &opts);
    program_name = basename(argv[0]);
    for (int argi = optind + 1; argi < argc; ++argi)
    {   //store user string into keyword
        keyword = argv[optind];
        char *filename = argv[argi];
        if (strcmp (filename, STDIN_NAME) == 0)
        {
            catbyline(stdin, &opts, filename, argv, argi);
        }else
        {   //If out file is not null then run
            FILE *input = fopen(filename, "r");
            if (input != NULL)
            {   //call catbyline
                catbyline(input, &opts, filename, argv, argi);
                fclose(input);
            }else
            {
                exit_status = EXIT_FAILURE;
                fflush(NULL);
                fprintf(stderr, "%s: %s: %s\n", program_name,
                        filename, strerror (errno));
                fflush(NULL);
            }
        }
    }   
    return exit_status;
}