//
//  double_linked_list.c
//  
//
//  Created by Brian Flores on 3/21/15.
//
//

#include <stdio.h>
#include <stdlib.h>

struct node
{
    int data;
    struct node* next;
    struct node* prev;
};

struct node* head;

struct node* new_node(int x)
{
    struct node* newNode
    = (struct node*)malloc(sizeof(struct node));
    
    newNode->data = x;
    newNode->prev = NULL;
    newNode->next = NULL;
    return newNode;
}

//Insert node
void insert_node(int x)
{
    struct node* t = new_node(x);
    if(head == NULL)
    {
        head = t;
        return;
    }
    head->prev = t;
    t->next = head;
    head = t;
}

//Inset a node at tail of Double linked list
void insert_tail(int x)
{
    struct node* tmp = head;
    struct node* newNode = new_node(x);
    if(head == NULL)
    {
        head = newNode;
        return;
    }while(tmp->next != NULL) tmp = tmp->next; //Go to last node
    tmp->next = newNode;
    newNode->prev = tmp;
}

//Prints all the elements in linked list in forward traversal order
void Print() {
    struct node* tmp = head;
    printf("Forward: ");
    while(tmp != NULL) {
        printf("%d ",tmp->data);
        tmp = tmp->next;
    }
    printf("\n");
}

//Prints all elements in linked list in reverse traversal order.
void ReversePrint() {
    struct node* tmp = head;
    if(tmp == NULL) return; // empty list, exit
    // Going to last Node
    while(tmp->next != NULL) {
        tmp = tmp->next;
    }
    // Traversing backward using prev pointer
    printf("Reverse: ");
    while(tmp != NULL) {
        printf("%d ",tmp->data);
        tmp = tmp->prev;
    }
    printf("\n");
}

int main() {
    
    /*Driver code to test the implementation*/
    head = NULL; // empty list. set head as NULL.
    
    // Calling an Insert and printing list both in forward as well as reverse direction.
    insert_tail(1); Print(); ReversePrint();
    insert_tail(2); Print(); ReversePrint();
    insert_node(3); Print(); ReversePrint();
    insert_tail(4); Print(); ReversePrint();
    
}









